# NSDS .Net Build

This is a framework for running .Net 6 based containers on OpenShift for NSDS. .Net 6 apps projects can be placed into this directory for running in Docker.


## How to use with .Net 6 

For reference the steps are:

1. Clone this repo locally
2. Change to the directory of where this repo is located and create a new project in its own subdirectory with the commandi
   * `dotnet new mvc -o WebApp -f net6.0 --no-restore` Note: this targets 6.0 framework
   * `dotnet new sln --name WebApp` creates a template solution
   * `dotnet sln WebApp.sln add WebApp/WebApp.csproj` Adds the project to the solution
3. Modify the Dockerfile for the appname as you created above.
4. Add the following lines to the `WebApp/WebApp.csproj` to generate a nuget [lockfile](https://devblogs.microsoft.com/nuget/enable-repeatable-package-restores-using-a-lock-file/) for the [GitLab Dependency Scanner](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/).
    ```c#
    <PropertyGroup>
        <RestorePackagesWithLockFile>true</RestorePackagesWithLockFile>
    </PropertyGroup>
    ```

### Adding the files to git

Then add the files to source control

```shell
git add .
git commit -m "starting"
git push
```

Note: to switch branches (ie cloning this repo and pushing to a new repo)

```shell
git remote remove origin
git remote add origin git@gitlab.novascotia.ca:group/some-new-site.git
git push --set-upstream origin main
```

### Git Branches

There are two branches to use for deploying to the environments.

* **dev**
  * Any active development should happen to this branch
* **main**
  * When a production environment is ready to be updated a merge request from dev -> main should take place to update the environment.

### Docker Build

The Dockerfile is from the [Microsoft samples](https://github.com/dotnet/dotnet-docker). In order to use it with an app you might need to customize the settings such as "WebApp" in the file.

The main difference from the Microsoft asp.net sample is the following lines:

```yaml
ENV ASPNETCORE_URLS=http://*:8080
EXPOSE 8080
```

before the ENTRYPOINT line. This allows for the container to start on 8080 vs 80 allowing for proper execution without root privileges.


### GitLab CI/CD

The [.gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/README.html) is pre-configured for your application to automatically scan and to deliver your code as a Container image to an internal registry called Harbor. From there communication is sent to the NSDS PaaS OpenShift to use this new image.

This file uses multiple CI/CD variables that are pre-configured by the NSDS PaaS team when your project is generated. If you have any [support issues](https://devops.novascotia.ca) reach out to the [NSDS DevOps team](https://devops.novascotia.ca).

The following CI/CD tokens are required in order for this process to succeed:

We recommmend the following at a project level:
* **IMAGE_NAME**
  * **testapp** this is the harbor image name to be used (all lowercase, no spaces or hyphens)
* **OC_APPNAME**
  * **testapp** this is the OpenShift deployment or deploymentconfig to be used (all lowercase, no spaces or hyphens)
* **DAST_WEBSITE**
  * **https://some-route.apps.nonprod-hfx1.novascotia.ca** this is the OpenShift route where your app is located for web based scanning

We recommend the following at a group level:

* **IMAGE_NAMESPACE**
  * **teamname** this is the harbor image namespace (all lowercase, no spaces or hyphens)
* **OC_NAMESPACE**
  * **teamname** this is the OpenShift namespace/project (all lowercase, no spaces or hyphens)
* **OPENSHIFT_DEV_URL**
  * **https://api.nonprod.novascotia.ca:6443** the OpenShift api endpoint of the environment
* **OPENSHIFT_DEV_TOKEN**
  * A service account token to start a new deployment in this environment
* **OPENSHIFT_PROD_URL**
  * **https://api.prod.novascotia.ca:6443** the OpenShift api endpoint of the environment
* **OPENSHIFT_PROD_TOKEN**
  * A service account token to start a new deployment in this environment
* **REGISTRY_HOST**
  * **harbor.novascotia.ca** the OCI image repository
* **REGISTRY_ROBOT**
  * A [harbor.novascotia.ca](https://harbor.novascotia.ca) robot user to publish OCI images
* **REGISTRY_TOKEN**
  * A [harbor.novascotia.ca](https://harbor.novascotia.ca) robot token to publish OCI images


### OpenShift PaaS

[OpenShift](https://www.openshift.com/) is the NSDS on-premise PaaS that provides a Kubernetes environment for application developers to host their code in a secure and multi-tenant environment.


## Directory structure

This folder has some pre-defined folders for usage:

| Folder name       | Description                |
|-------------------|----------------------------|
| `./`              | Application source code |


## Support

The [NSDS DevOps team](https://devops.novascotia.ca) supports this image and the build process using this container. Security and support of your application code and framework falls to the application team and/or end user.

Please feel free to send any merge requests or pulls to this code base.
